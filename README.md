Fill Murray Browser Extension for Chrome
========================================

Browser extension for Chrome (or Chromium) that will replace all images on a page with images of Bill Murray. The specific image used is determined by the size of the original image. Made as part of an introductory workshop on making browser extensions.

Before
------
![Screenshot of YouTube homepage.](before.jpg)

After
-----
![Screenshot of YouTube Homepage with extension applied. All images replaced with photos of Bill Murray.](after.jpg)
