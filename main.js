// document.body.style.backgroundColor = "yellow";
// document.body.style.transition = "all 1s ease-in-out"
//
//
// let colors = ["yellow", "cyan", "hotpink", "lightgreen"];
// let index = 1;
// let rotation = 0
//
// setInterval(function () {
//   document.body.style.backgroundColor = colors[index++ % colors.length];
//   document.body.style.transform = "rotate(" + (rotation += 5) + "deg)";
// }, 1000)

let allImages = document.getElementsByTagName('img');

for (let i = 0; i < allImages.length; i++) {
  let imageHeight = allImages[i].clientHeight || allImages[i].height || 300;
  let imageWidth = allImages[i].clientWidth || allImages[i].width || 300;

  allImages[i].src = "//fillmurray.com/" + imageWidth + "/" + imageHeight + "/";
}
